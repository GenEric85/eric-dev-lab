﻿using Microsoft.AspNetCore.Mvc;

namespace TestDockerAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class RandomNumberController : ControllerBase
    {
        [HttpGet]
        public int RandomNumber(int fromInclusinve = 0, int toExclusive = 1000) => System.Security.Cryptography.RandomNumberGenerator.GetInt32(fromInclusinve, toExclusive);
    }
}
